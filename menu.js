const hamburger = document.querySelector(".menu_hamburger");
const navmenu = document.querySelector(".menu_nav_menu");


hamburger.addEventListener("click", mobileMenu);

function mobileMenu() {
   hamburger.classList.toggle("active");
    navmenu.classList.toggle("active");
}

const navLink = document.querySelectorAll(".menu_nav_link");

navLink.forEach(n => n.addEventListener("click", closeMenu));

function closeMenu() {
    hamburger.classList.remove("active");
    navmenu.classList.remove("active");
}